'use strict';

// 1 chapter: objects literal

var customer = {
  name: 'Tom Smith',
  speak: function() {
    return 'My name is ' + this.name;
  },
  address: {
    street: '123 Main St',
    city: 'Pittsburg',
    state: 'PA'
  }
}

document.write(customer.speak() + '<br>');
document.write(customer.name + ' lives at '+ customer.address.street + '<br>');

customer.address.country = 'US';

document.write(customer.address.country + '<br>');

function Person(name, street) {
  this.name = name;
  this.street = street;

  this.info = function() {
    return 'My name is ' + this.name + ' and I live on '+ this.street;
  }
}

var bobSmith = new Person('Bob smith', '234 main st');

document.write(bobSmith.info() + '<br>');

document.write('Bob Smith is a Person: ' + (bobSmith instanceof Person) + '<br>');


function changeName(person) {
  person.name = "Sue Smith";
}

changeName(bobSmith);

document.write('Bob became ' + bobSmith.name + '<br>');

var person1 = new Person("Paul", "123 Main");
var person2 = new Person("Paul", "123 Main");

document.write('Are they equal ' + (person1 == person2) + '<br>');

// 2 chapter: prototypes

function getSum(num1, num2) {
  return num1 + num2;
}

document.write("Num if arguments "+ getSum.length + "<br>");

function Mammal(name) {
  this.name = name;
  this.getInfo = function() {
    return "The mammals name is " + this.name;
  }
}

Mammal.prototype.sound = "Grrr";

Mammal.prototype.makeSound = function() {
  return this.name + " says " + this.sound;
}

var grover = new Mammal("Grover");

document.write(grover.makeSound() + "<br>");

for(var prop in grover) {
  document.write(prop + " : " + grover[prop]  +"<br>");
}

for(var prop in grover) {
  if(grover.hasOwnProperty(prop)) {
    document.write("obj-> " + prop + " : " + grover[prop]  +"<br>");
  } else {
    document.write("prototype-> " + prop + " : " + grover[prop]  +"<br>");
  }
}

Array.prototype.inArray = function inArray(value) {
  for(var i = 0; i < this.length; i++) {
    if(this[i] == value) {
      return true;
    }
  }
  return false;
}

var sampArr = [1,2,3,4,5];

document.write("3 in sampArr: " + sampArr.inArray(3) + "<br>");

// 3 chaprter: private properties

function SecretCode() {
  var sercretNumber = 78;

  this.guessNum = function(num) {
    if(num > sercretNumber) {
      return "lower";
    } else if (num < sercretNumber) {
      return "Higher";
    } else {
      return "You guessed it";
    }
  }
}

SecretCode.prototype.getSecret = function() {
  return this.sercretNumber;
}

var secret = new SecretCode();

document.write("Value of sercretNumber: " + secret.sercretNumber + "<br>");

document.write("Is 70 the number: " + secret.guessNum(70) + "<br>");

document.write("The secret number is : " + secret.getSecret() + "<br>");

// 4 chapter: getters, setters

var address = {
  street: "No Street",
  city: "No City",
  state: "No State",

  get address() {
    return this.street + "; " + this.city + "; "+ this.state;
  },

  set address(theAddress) {
    var parts = theAddress.toString().split(", ");
    this.street = parts[0] || "";
    this.city = parts[1] || "";
    this.state = parts[2] || "";
  }
}


address.address = "123 Main St, Pittsburgh, PA";

document.write("" + address.address + "<br>");

function Coordinates() {
  this.latitude = 0;
  this.longtitude = 0;
}

Object.__defineGetter__.call(Coordinates.prototype, "getCoords", function() {
  return "lat: " + this.latitude + " Long : " + this.longtitude;
});

Object.__defineSetter__.call(Coordinates.prototype, "setCoords", function(coords) {
  var parts = coords.toString().split(", ");
  this.latitude = parts[0] || "";
  this.longtitude = parts[1] || "";
});

var testCoords = new Coordinates();

testCoords.setCoords = "40.24, 56.72";

document.write("Coordinates: " + testCoords.getCoords + "<br>");

function Point1() {
  this.x = 0;
  this.y = 0;
}

Object.defineProperty(Point1.prototype, "pointPos", {
  get: function() {
    return "X: " + this.x + " Y: " + this.y;
  },
  set: function(thePoint) {
    var parts = thePoint.toString().split(", ");
    this.x = parts[0] || '';
    this.y = parts[1] || '';
  }
});

var aPoint = new Point1();

aPoint.pointPos = "100, 200";
document.write("Point position" + aPoint.pointPos + "<br>");

// 5 chapter: ES5

var Circle = function(radius) {
  this._radius = radius;
}

Circle.prototype = {
  set radius(radius) { this._radius = radius; },
  get radius() { return this._radius; },
  get area() { return Math.PI * (this._radius * this._radius); }
}

var circ = new Circle(10);

circ.radius = 15;

document.write("A circle with radius" + circ.radius + " has an area of "  + circ.area.toFixed()  + "<br>");

// 6 chapter: inheritance
function Animal() {
  this.name = "Animal";

  this.toString = function() {
    return "My name is " + this.name;
  }
}

function Canine() {
  // this.name = "Canine";
}

function Wolf() {
  // this.name = "Wolf";
}

Canine.prototype = new Animal();
Wolf.prototype = new Canine();
Canine.prototype.constructor = Canine;
Wolf.prototype.constructor = Wolf;

var arcticWolf = new Wolf();

document.write(arcticWolf.toString() + "<br>");
document.write( "Wolf instance of Animal: " + (arcticWolf instanceof Animal) + "<br>");

Animal.prototype.sound = "Grrr";
Animal.prototype.getSound = function() {
  return this.name + " says " + this.sound;
}

Canine.prototype.sound = "Woof";
Wolf.prototype.sound = "Grrr Woof";
document.write( arcticWolf.getSound() + "<br>");

function Rodent() {
  this.name = "Rodent";
}

function Rat() {
  this.name = "Rat";
}

Rodent.prototype = new Animal();
Rat.prototype = Rodent.prototype;
Rodent.prototype.constructor = Rodent;
Rat.prototype.constructor = Rat;

var caneRat = new Rat();
document.write(caneRat.toString() + "<br>");

// 7 chapter: extend

function extend(Child, Parent) {
  var F = function() {};

  F.prototype = Parent.prototype;

  Child.prototype = new F();

  Child.prototype.constructor = Child;
}

function Deer() {
  this.name = "Deer";
  this.sound = "Short";
}

extend(Deer, Animal);

var elk = new Deer();

document.write(elk.getSound() + "<br>");

// 8 chapter: superclass methods call/apply
function Vehicle(name) {
  this.name = "Vehicle";
}

Vehicle.prototype = {
  drive: function() {
    return this.name + " drives forward";
  },
  stop: function() {
    return this.name + " stops";
  }
}

function Truck(name) {
  this.name = name;
}

Truck.prototype = new Vehicle();
Truck.prototype.constructor = Truck;

Truck.prototype.drive = function() {
  var driveMessage = Vehicle.prototype.drive.apply(this);
  return driveMessage += " through a field";
}

var jeep = new Truck("Jeep");

document.write(jeep.drive()+"<br>");
document.write(jeep.stop()+"<br>");

// 9 chapter: ES6 method defining
var addStuff = {
  sum: function(num1, num2) {
    return num1 + num2;
  }
}

document.write("1 + 2 = ", addStuff.sum(1,2), "<br>");

var addStuff = {
  sum(num1, num2) {
    return num1 + num2;
  }
}
document.write("1 + 2 = ", addStuff.sum(1,2), "<br>");

// 9 chapter: ES6 classes
function Point2(x, y) {
  this.x = x;
  this.y = y;
}

Point2.prototype.getPos = function() {
  return "X: " + this.x + " Y: " + this.y;
}

var point2 = new Point2(100, 200);

document.write("Point Pos: " + point2.getPos() + "<br>");

class Point3 {
  constructor(x, y) {
    this.x = x || 0;
    this.y = y || 0;
  }

  getPos(){
    return "X: " + this.x + " Y: " + this.y;
  }
}

var point3 = new Point3(1, 2);
document.write("Point Pos: " + point3.getPos() + "<br>");

class AnimalNew {
  constructor(name) {
    this.name = name;
  }

  toString() {
    return "Animal is name " + this.name;
  }

  static getAnimal() {
    return new AnimalNew("No Name");
  }
}

class Dog extends AnimalNew {
  constructor(name, owner) {
    super(name);
    this.owner = owner;
  }

  toString() {
    return super.toString() + "<br>Dog is named " + this.name;
  }
}

var rover = new Dog("Rover", "Paul");

document.write(rover.name + " is owned by " + rover.owner + "<br>");
document.write(rover.toString() + "<br>");

var bowser = AnimalNew.getAnimal();

document.write("Bowser info: " + bowser.toString());


// 10 chapter: singleton pattern

function Hero(name) {
  if(typeof Hero.instance === "object") {
    return Hero.instance;
  }
  var hero = {};
  hero.name = name;
  Hero.instance = hero;
  return hero;
}

var derekHero = Hero("Derek");
document.write("Our here is "+ derekHero.name + "<br>");
var paulHero = Hero("Paul");
document.write("Our here is "+ derekHero.name + "<br>");
// 11 chapter: factory pattern

function Sword(desc) {
  this.weaponType = "Sword";
  this.metal = desc.metal || "Steel";
  this.style = desc.style || "Longsword";
  this.hasMagic = desc.hasMagic || false;
}

function Bow(desc) {
  this.weaponType = "Bow";
  this.metal = desc.metal || "Wood";
  this.style = desc.style || "Longbow";
  this.hasMagic = desc.hasMagic || false;
}

function WeaponFactory(){};

WeaponFactory.prototype.makeWeapon = function(desc) {
  var weaponClass = null;

  if(desc.weaponType === "Sword") {
    weaponClass = Sword;
  } else if (desc.weaponType === "Bow") {
    weaponClass = Bow;
  } else {
    return false;
  }

  return new weaponClass(desc);
}

var myWeaponFactory = new WeaponFactory();

var bladeFist = myWeaponFactory.makeWeapon({
  weaponType: "Sword",
  metal: "Dark Iron",
  style: "Scythe",
  hasMagic: true
});

document.write(bladeFist.weaponType + " of type " + bladeFist.style + " crafted from " + bladeFist.metal + "<br>");

// 12 chapter: decorator pattern
function Pizza(price) {
  this.price = price || 10;
}

Pizza.prototype.getPrice = function() {
  return this.price;
}

function ExtraCheese(pizza) {
  var prevPrice = pizza.price;

  pizza.price = prevPrice + 1;
}

var myPizza = new Pizza(10);

ExtraCheese(myPizza);

document.write("Cost of Pizza : $" + myPizza.getPrice() + "<br>");

// 13 chapter: observer pattern
var Observable = function(){
  this.subscribers = [];
}

Observable.prototype = {
  subscribe: function(subscriber) {
    for(var i = 0; i < this.subscribers.length; i++) {
      if(this.subscribers[i] === subscriber) {
        return false;
      }
    }
    this.subscribers.push(subscriber);
  },

  unsubscribe: function(unsubscriber) {
    for(var i = 0; i < this.subscribers.length; i++) {
      if(this.subscribers[i] === unsubscriber) {
        this.subscribers.splice(i, 1);
        return unsubscriber.name;
      }
    }
  },

  publish: function(data) {
    for(var i = 0; i < this.subscribers.length; i++) {
      this.subscribers[i].receiveData(data);
    }
  }
}

var OrganFanny = {
  name: "Organ Fanny",
  receiveData: function(data) {
    document.write(this.name + " received your info: " + data + "<br>");
  }
}

var BoldmanYaks = {
  name: "Boldman Yaks",
  receiveData: function(data) {
    document.write(this.name + " received your info: " + data + "<br>");
  }
}


var observable = new Observable();
observable.subscribe(OrganFanny);
observable.subscribe(BoldmanYaks);

observable.publish("IBM at $ 145.30");
observable.unsubscribe(OrganFanny);
observable.publish("IBM at $ 144.15");